# Manage your todos

[![asciicast](https://asciinema.org/a/507402.svg)](https://asciinema.org/a/507402)

List todos (grouped, filtered)
todos [--groub_by=project|context|due_date|status]
Subcommands:
- init       -> todos init [path]
- add        -> todos add 'This is a text to include' [options]
- archive    -> todos archive \<id\>
- unarchive  -> todos unarchive \<id\>
- remove     -> todos remove \<id\> [id2,...,idn] [options]
- edit       -> todos edit \<id\> 'New todo subject'
- complete   -> todos complete \<id1\> [id2,...,idn]
- uncomplete -> todos uncomplete \<id1\> [id2,...,idn]
- notes:
   - add notes    -> todos \<id\> notes add 'This is a note'
   - remove notes -> todos \<id\> notes remove \<noteid\>
   - edit notes   -> todos \<id\> notes edit \<noteid\>
