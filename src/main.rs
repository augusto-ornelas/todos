// TODO: Examples

// Listing all todos
// [X] todos

// Grouping:
// [X] todos --group_by=project  | -g project
// [X] todos --group_by=context  | -g context
// [X] todos --group_by=status   | -g status
// [ ] todos --group_by=due_date | -g due_date

// Filtering:
// [X] todos --project=<project_name>|-p <projec_name>
// [ ] todos --project=!<project_name>|-p !<project_name> ; list all the todos except the ones from <project_name>
// [X] todos --completed | -C
// [X] todos --context=<context> | -c <context>
// [ ] todos --context=!<context> | -c !<context> ; list all the todos except the ones from with context
// [ ] todos --due=today|tomorrow|week|month | -d <date>
// TODO: I have to think about this one better:
// What are the posible status? completed,uncompleted,archived? Because those exit already
// or do another alternative is to add a new status field in the .todos.json file and
// filter by those status: which could include anything the user wants:
// I would use it for something like now,next,later for example
// [ ] todos --status=<status> | -s <status>

// You can also combine group_by and filter:
// [ ] todos -g context -p !work

// Edit todos
// [X] todos edit 3 <subject> [--due=date --project=[!]project1,..,projectn --context=context1,..,contextn]
// [ ] add, remove or override projects and contexts [add->+ remove->- override=noprefix]

// Add todos
// [ ] todos add <subject> [--due=date --project=project1,..,projectn --context=context1,..,contextn]
// Operating in notes
// [ ] todos <id> notes (add|remove|edit)

// List todos (grouped, filtered)
// todos [--groub_by=project|context|due_date|status]
// Subcommands:
//    init       -> todos init [path]
//    add        -> todos add 'This is a text to include' [options]
//    archive    -> todos archive <id>
//    unarchive  -> todos unarchive <id>
//    remove     -> todos remove <id> [id2,...,idn] [options]
//    edit       -> todos edit <id> 'New todo subject'
//    complete   -> todos complete <id1> [id2,...,idn]
//    uncomplete -> todos uncomplete <id1> [id2,...,idn]
//    notes:
//        add notes    -> todos <id> notes add 'This is a note'
//        remove notes -> todos <id> notes remove <noteid>
//        edit notes   -> todos <id> notes edit <noteid>
// Change the status of todos
// TODO: Priority as a number from 0 to 9 (0 highest priority)?
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::error::Error;
use std::{fmt, fs};

fn is_stdout_tty() -> bool {
    unsafe { libc::isatty(libc::STDOUT_FILENO) != 0 }
}

fn get_terminal_size() -> Window {
    let win = Window::default();
    unsafe { libc::ioctl(0, libc::TIOCGWINSZ, &win) };
    win
}

#[derive(Debug, Default)]
#[allow(dead_code)]
struct Window {
    h: u16,
    w: u16,
}

#[derive(Default, Clone, PartialEq, Eq, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct TodoItem {
    archived: bool,
    completed: bool,
    completed_date: String,
    contexts: Vec<String>,
    due: String,
    id: u64,
    is_priority: bool,
    notes: Option<Vec<String>>,
    projects: Vec<String>,
    subject: String,
}

// TODO: set a limit to the lenght of the todo subjects
// for security and display reasons (a subject that needs to wrap around in the terminal it might not be practicle)
fn add_todo(
    todos: &mut Vec<TodoItem>,
    subject: String,
    projects: Vec<String>,
    contexts: Vec<String>,
) {
    todos.push(TodoItem {
        id: next_id(&todos.iter().map(|x| x.id).collect::<Vec<u64>>()),
        subject,
        is_priority: false,
        archived: false,
        projects,
        completed: false,
        completed_date: String::new(),
        contexts,
        due: String::new(),
        notes: None,
    })
}

fn find_mut_todo(todos: &mut Vec<TodoItem>, id: u64) -> Result<&mut TodoItem, String> {
    todos
        .iter_mut()
        .find(|x| x.id == id)
        .ok_or(format!("Could not find any todo with the id '{}'", id))
}

// TODO: implement all the filters
// [X] by project
// [X] by context
// [X] by completed
// [ ] by due date, TODO: I have to bring in a datetime library
fn filter_todos(todos: &[TodoItem], flags: &Flags) -> Vec<TodoItem> {
    todos
        .iter()
        .filter(|x| {
            (flags.projects.is_none()
                || x.projects
                    .iter()
                    .any(|proj| flags.projects.as_ref().unwrap().iter().any(|p| proj == p)))
                && (flags.contexts.is_none()
                    || x.contexts
                        .iter()
                        .any(|ctx| flags.contexts.as_ref().unwrap().iter().any(|c| ctx == c)))
                && (flags.completed.is_none() || x.completed == flags.completed.unwrap())
                && (x.archived == flags.archived
                    || flags.group_by.is_some()
                        && flags.group_by.as_ref().unwrap() == &Group::Status)
        })
        .cloned()
        .collect()
}

fn next_id(ids: &[u64]) -> u64 {
    for i in 1..ids.len() {
        for (j, id) in ids.iter().enumerate() {
            if i as u64 == *id {
                break;
            }
            if i as u64 != *id && j == ids.len() - 1 {
                return i as u64;
            }
        }
    }
    ids.len() as u64 + 1
}

fn group_by(todos: &[TodoItem], group: Option<Group>) -> BTreeMap<String, Vec<&TodoItem>> {
    let mut grouped: BTreeMap<String, Vec<&TodoItem>> = BTreeMap::new();
    match group {
        Some(Group::Context) => {
            let mut contexts: Vec<String> =
                todos.iter().map(|x| x.contexts.clone()).flatten().collect();
            contexts.sort();
            contexts.dedup();
            for context in contexts.iter() {
                grouped.insert(
                    context.clone(),
                    todos
                        .iter()
                        .filter(|x| x.contexts.iter().any(|y| y == context.as_str()))
                        .collect(),
                );
            }
            let without_context: Vec<&TodoItem> =
                todos.iter().filter(|x| x.contexts.is_empty()).collect();
            if !without_context.is_empty() {
                grouped.insert("No context".into(), without_context);
            }
        }
        Some(Group::Project) => {
            let mut projects: Vec<String> =
                todos.iter().map(|x| x.projects.clone()).flatten().collect();
            projects.sort();
            projects.dedup();
            for project in projects.iter() {
                grouped.insert(
                    project.clone(),
                    todos
                        .iter()
                        .filter(|x| x.projects.iter().any(|y| y == project.as_str()))
                        .collect(),
                );
            }
            let without_project: Vec<&TodoItem> =
                todos.iter().filter(|x| x.projects.is_empty()).collect();
            if !without_project.is_empty() {
                grouped.insert("No project".into(), without_project);
            }
        }
        Some(Group::Status) => {
            grouped.insert(
                "Completed".into(),
                todos
                    .iter()
                    .filter(|x| x.completed && !x.archived)
                    .collect(),
            );
            grouped.insert(
                "Uncompleted".into(),
                todos
                    .iter()
                    .filter(|x| !x.completed && !x.archived)
                    .collect(),
            );
            grouped.insert(
                "Archived".into(),
                todos.iter().filter(|x| x.archived).collect(),
            );
        }
        None => {
            grouped.insert("all".into(), todos.iter().collect());
        }
        _ => {}
    }
    grouped
}

const BLUE: &str = "\x1B[34m";
const CYAN: &str = "\x1B[36m";
const MAGENTA: &str = "\x1B[35m";
const DEFAULT: &str = "\x1B[39m";
const YELLOW: &str = "\x1B[33m";
const GREEN: &str = "\x1B[32m";

fn print_full_decoration_groups(groups: &BTreeMap<String, Vec<&TodoItem>>, with_notes: bool) {
    let cols = get_terminal_size().w as usize;
    for (group_name, todos) in groups {
        println!("┌─{}─┐", "─".repeat(group_name.len()));
        println!("│ {}{}{} │", BLUE, &group_name, DEFAULT);
        println!(
            "├─{}─┴{}┐",
            "─".repeat(group_name.len()),
            "─".repeat(cols - group_name.len() - 5)
        );
        for (i, todo) in todos.iter().enumerate() {
            print!(
                "│{}{:>4}{}\t[{}{}{}]",
                YELLOW,
                todo.id,
                DEFAULT,
                GREEN,
                if todo.completed { 'X' } else { ' ' },
                DEFAULT,
            );
            fold(&todo.subject, cols - 15)
                .iter()
                .enumerate()
                .for_each(|(j, y)| {
                    println!(
                        "{}{:<2$} │",
                        if j > 0 { "│\t    " } else { " " },
                        y,
                        cols - 14
                    )
                });
            if with_notes {
                print_notes(todo.notes.as_ref().unwrap_or(&Vec::new()), cols);
            }
            if i != todos.len() - 1 {
                println!("├{}┤", "─".repeat(cols - 2));
            }
        }
        print!("└{}┘", "─".repeat(cols - 2));
    }
}

fn print_simple_without_color(
    groups: &BTreeMap<String, Vec<&TodoItem>>,
    with_notes: bool,
    cols: usize,
) {
    for (group_name, todos) in groups {
        println!(" {}", group_name);
        println!("{}", TodoList(todos.iter().cloned().cloned().collect()));
        if with_notes {
            todos
                .iter()
                .for_each(|x| print_notes(x.notes.as_ref().unwrap_or(&Vec::new()), cols));
        }
    }
}

fn print_notes(notes: &[String], cols: usize) {
    if notes.is_empty() {
        return;
    }
    println!("├{}┤", "─".repeat(cols - 2));
    println!("│ {}Notes{} {:<3$} │", CYAN, DEFAULT, ' ', cols - 10);
    println!("├{}┤", "─".repeat(cols - 2));
    notes.iter().enumerate().for_each(|(i, x)| {
        print!("│{}{:>4}{}", MAGENTA, i, DEFAULT,);
        fold(x, cols - 11)
            .iter()
            .enumerate()
            // .inspect(|y| println!("{}", y))
            .for_each(|(j, y)| {
                println!("{}\t{:<2$} │", if j > 0 { '│' } else { ' ' }, y, cols - 10)
            })
    });
}

fn print_simple_with_color(groups: &BTreeMap<String, Vec<&TodoItem>>) {
    for (group_name, todos) in groups {
        println!(" {}{}{}", BLUE, group_name, DEFAULT);
        println!(
            "{}",
            ColorTodoList(todos.iter().cloned().cloned().collect())
        );
    }
}

fn fold(note: &str, limit: usize) -> Vec<String> {
    // TODO: come back to clean up this mess
    let mut result = Vec::new();
    if note.len() < limit {
        result.push(note.to_owned());
        return result;
    }
    let mut len = 0;
    let mut line = String::new();
    for word in note.split_whitespace() {
        if len + word.len() < limit && word.len() < limit {
            len += word.len() + 1;
            line.push_str(word);
            line.push(' ');
        } else if word.len() >= limit {
            if len > 0 {
                result.push(line.clone());
            }
            let mut subword = word.to_owned();
            while subword.len() > limit {
                let mut chars = subword.chars().collect::<Vec<_>>();
                let rest = chars.split_off(limit);
                line = chars.iter().collect();
                line.push('…');
                result.push(line.clone());
                line.drain(..);
                subword = rest.iter().collect();
            }
            if !subword.is_empty() {
                result.push(subword.clone() + " ");
                line.drain(..);
            }
            len = 0;
        } else {
            result.push(line.clone());
            line.drain(..);
            line.push_str(word);
            line.push(' ');
            len = word.len() + 1;
        }
    }
    if !line.is_empty() {
        result.push(line.clone());
    }
    result
}

use chrono::prelude::*;

fn main() -> Result<(), Box<dyn Error>> {
    let (args, flags) = Flags::from_args(&mut std::env::args())?;
    let width = get_terminal_size().w as usize;
    let mut dir = fs::canonicalize(".")?;
    while dir.to_string_lossy().len() >= std::env::var("HOME")?.len() {
        let found = fs::read_dir(&dir)?
            .map(|res| res.map(|e| e.path()))
            .any(|f| f.unwrap().to_str().unwrap().ends_with("/.todos.json"));
        if found {
            break;
        }
        let path = dir.to_string_lossy();
        let mut folders = path.split('/').collect::<Vec<_>>();
        folders.drain(folders.len() - 1..folders.len());
        dir = std::path::PathBuf::from(folders.join("/"));
    }

    let path = format!("{}/.todos.json", dir.to_string_lossy());
    let file_content = fs::read_to_string(&path)?;
    let mut todos: Vec<TodoItem> = serde_json::from_str(&file_content).unwrap_or_default();

    let subcommand = args.get(1);

    if subcommand.is_none() {
        let todos = filter_todos(&todos, &flags);
        let grouped = group_by(&todos, flags.group_by);
        match flags.color {
            ColorMode::Auto => {
                if is_stdout_tty() {
                    print_full_decoration_groups(&grouped, flags.with_notes);
                } else {
                    print_simple_without_color(&grouped, flags.with_notes, width);
                }
            }
            ColorMode::Always => {
                if is_stdout_tty() || flags.decoration == DecorationMode::Full {
                    print_full_decoration_groups(&grouped, flags.with_notes);
                } else {
                    print_simple_with_color(&grouped)
                }
            }
            ColorMode::Never => {
                print_simple_without_color(&grouped, flags.with_notes, width);
            }
        }
        return Ok(());
    }

    match subcommand.unwrap().as_str() {
        "init" => {
            let path = args.get(2);
            match path {
                None => {
                    fs::File::create(".todos.json")?;
                }
                Some(path) => {
                    let path = fs::canonicalize(path)?;
                    fs::File::create(format!("{}/.todos.json", path.to_string_lossy()))?;
                }
            }
            return Ok(());
        }
        "add" | "a" => {
            add_todo(
                &mut todos,
                args.get(2).ok_or("Not enough arguments")?.into(),
                flags.projects.unwrap_or_default(),
                flags.contexts.unwrap_or_default(),
            );
        }
        "remove" | "rm" => {
            let id = args.get(2).ok_or("Not enough arguments")?.parse::<u64>()?;
            todos = todos.iter().filter(|x| x.id != id).cloned().collect();
        }
        "complete" | "c" => {
            let id = args.get(2).ok_or("Not enough arguments")?.parse::<u64>()?;
            let mut todo = find_mut_todo(&mut todos, id)?;
            (*todo).completed = true;
            (*todo).completed_date = Local::now().to_rfc3339();
        }
        "uncomplete" | "uc" => {
            let id = args.get(2).ok_or("Not enough arguments")?.parse::<u64>()?;
            let todo = find_mut_todo(&mut todos, id)?;
            (*todo).completed = false;
            (*todo).completed_date = String::new();
        }
        "edit" | "e" => {
            let id = args.get(2).ok_or("Not enough arguments")?.parse::<u64>()?;
            let todo = find_mut_todo(&mut todos, id)?;
            if let Some(subject) = args.get(3) {
                (*todo).subject = subject.into();
            }
            if let Some(projects) = flags.projects {
                (*todo).projects = projects;
            }
            if let Some(contexts) = flags.contexts {
                (*todo).contexts = contexts;
            }
            if let Some(date) = flags.due {
                (*todo).due = date;
            }
        }
        "archive" | "ar" => {
            let id = args.get(2).ok_or("Not enough arguments")?.parse::<u64>()?;
            let todo = find_mut_todo(&mut todos, id)?;
            (*todo).archived = true;
        }
        "unarchive" | "uar" => {
            let id = args.get(2).ok_or("Not enough arguments")?.parse::<u64>()?;
            let todo = find_mut_todo(&mut todos, id)?;
            (*todo).archived = false;
        }
        "prioritize" | "p" => {
            let id = args.get(2).ok_or("Not enough arguments")?.parse::<u64>()?;
            let todo = find_mut_todo(&mut todos, id)?;
            (*todo).is_priority = true;
        }
        "deprioritize" | "dp" => {
            let id = args.get(2).ok_or("Not enough arguments")?.parse::<u64>()?;
            let todo = find_mut_todo(&mut todos, id)?;
            (*todo).is_priority = false;
        }
        possible_id => {
            let id = possible_id.parse::<u64>()?;
            if args.get(2).ok_or("Not enough arguments")? != "notes" {
                return Err(format!("{}: Unknown command parameter '{}'.\n\tUsage: {0} <id> notes add|remove|edit <args>",
                    args[0], args[2]).into());
            }

            match args.get(3).ok_or("Not enough arguments")?.as_str() {
                "add" => {
                    let todo = find_mut_todo(&mut todos, id)?;
                    let note = args.get(4).ok_or("Not enough arguments")?;
                    if todo.notes.is_none() {
                        (*todo).notes = Some(Vec::new());
                    }
                    (*todo).notes.as_mut().unwrap().push(note.into());
                }
                "remove" => {
                    let note_id = args.get(4).ok_or("Not enough arguments")?.parse::<u64>()?;
                    let todo = find_mut_todo(&mut todos, id)?;
                    if todo.notes.is_none()
                        || note_id as usize >= todo.notes.as_ref().unwrap().len()
                    {
                        return Err(format!(
                            "Could not find a todo with the note index {}",
                            note_id
                        )
                        .into());
                    }
                    (*todo).notes.as_mut().unwrap().remove(note_id as usize);
                }
                "edit" => {
                    let note_id = args.get(4).ok_or("Not enough arguments")?.parse::<u64>()?;
                    let todo = find_mut_todo(&mut todos, id)?;
                    if todo.notes.is_none()
                        || note_id as usize >= todo.notes.as_ref().unwrap().len()
                    {
                        return Err(format!(
                            "Could not find a todo with the note index {}",
                            note_id
                        )
                        .into());
                    }
                    let note = (*todo)
                        .notes
                        .as_mut()
                        .unwrap()
                        .get_mut(note_id as usize)
                        .ok_or("Note not found")?;
                    (*note) = args.get(5).ok_or("Not enough arguments")?.into();
                }
                arg => {
                    return Err(format!("Unknown command parameter '{}'.", arg).into());
                }
            }
        }
    }
    let data = serde_json::to_string(&todos)?;
    fs::write(path, data.as_bytes())?;
    Ok(())
}

struct ColorTodoList(Vec<TodoItem>);
impl fmt::Display for ColorTodoList {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (i, todo) in self.0.iter().enumerate() {
            write!(
                f,
                "{}{:>4}{}\t[{}{}{}] {}",
                YELLOW,
                todo.id,
                DEFAULT,
                GREEN,
                if todo.completed { 'X' } else { ' ' },
                DEFAULT,
                todo.subject
            )?;
            if i != self.0.len() - 1 {
                writeln!(f)?;
            }
        }
        Ok(())
    }
}

struct TodoList(Vec<TodoItem>);
impl fmt::Display for TodoList {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (i, todo) in self.0.iter().enumerate() {
            write!(
                f,
                "{:>4}\t[{}] {}",
                todo.id,
                if todo.completed { 'X' } else { ' ' },
                todo.subject
            )?;
            if i != self.0.len() - 1 {
                writeln!(f)?;
            }
        }
        Ok(())
    }
}

#[derive(Debug, PartialEq, Eq)]
enum Group {
    Project,
    Context,
    Status,
    Due,
}

#[derive(Debug, PartialEq, Eq)]
enum ColorMode {
    Auto,
    Always,
    Never,
}

#[derive(Debug, PartialEq, Eq)]
enum DecorationMode {
    Auto,
    Full,
    None,
}

impl std::default::Default for ColorMode {
    fn default() -> Self {
        ColorMode::Auto
    }
}

impl std::default::Default for DecorationMode {
    fn default() -> Self {
        DecorationMode::Auto
    }
}

#[derive(Default, Debug, PartialEq, Eq)]
struct Flags {
    projects: Option<Vec<String>>,
    color: ColorMode,
    decoration: DecorationMode,
    with_notes: bool,
    contexts: Option<Vec<String>>,
    completed: Option<bool>,
    due: Option<String>,
    archived: bool,
    status: Option<Vec<String>>,
    group_by: Option<Group>,
}

impl Flags {
    fn from_args(
        args: &mut (dyn std::iter::Iterator<Item = String> + 'static),
    ) -> Result<(Vec<String>, Flags), String> {
        let mut flags = Flags::default();
        let mut args_without_switches = Vec::new();
        while let Some(arg) = args.next() {
            if !arg.starts_with('-') {
                args_without_switches.push(arg);
            } else if arg.starts_with("--p") || arg == "-p" {
                let project = if arg.chars().any(|x| x == '=') {
                    arg.split('=').nth(1).unwrap().to_owned()
                } else {
                    args.next().ok_or("Not enough arguments")?
                };
                let projects = if project.chars().any(|x| x == ',') {
                    project.split(',').map(|x| x.to_string()).collect()
                } else {
                    vec![project]
                };
                flags.projects = Some(projects);
            } else if arg.starts_with("--con") || arg == "-c" {
                let context = if arg.chars().any(|x| x == '=') {
                    arg.split('=').nth(1).unwrap().to_owned()
                } else {
                    args.next().ok_or("Not enough arguments")?
                };
                flags.contexts = Some(vec![context]);
            } else if arg.starts_with("--com") || arg == "-C" {
                flags.completed = Some(true);
            } else if arg.starts_with("--a") || arg == "-a" {
                flags.archived = true;
            } else if arg.starts_with("--group_by") || arg == "-g" {
                let group = if arg.chars().any(|x| x == '=') {
                    arg.split('=').nth(1).unwrap().to_owned()
                } else {
                    args.next().ok_or("Not enough arguments")?
                };
                if group.starts_with('p') {
                    flags.group_by = Some(Group::Project)
                } else if group.starts_with('c') {
                    flags.group_by = Some(Group::Context)
                } else if group.starts_with('s') {
                    flags.group_by = Some(Group::Status)
                } else if group.starts_with('d') {
                    flags.group_by = Some(Group::Due)
                } else {
                    return Err(format!("Unknown group '{}'", group));
                }
            } else if arg.starts_with("--due") || arg == "-d" {
                let date = if arg.chars().any(|x| x == '=') {
                    arg.split('=').nth(1).unwrap().to_owned()
                } else {
                    args.next().ok_or("Not enough arguments")?
                };
                flags.due = Some(date);
            } else if arg.starts_with("--color") {
                let color_mode = if arg.chars().any(|x| x == '=') {
                    arg.split('=').nth(1).unwrap().to_owned()
                } else {
                    args.next().ok_or("Not enough arguments")?
                };
                flags.color = match color_mode.as_str() {
                    "always" => ColorMode::Always,
                    "never" => ColorMode::Never,
                    _ => ColorMode::Auto,
                }
            } else if arg.starts_with("--deco") {
                let decoration_mode = if arg.chars().any(|x| x == '=') {
                    arg.split('=').nth(1).unwrap().to_owned()
                } else {
                    args.next().ok_or("Not enough arguments")?
                };
                flags.decoration = match decoration_mode.as_str() {
                    "none" => DecorationMode::None,
                    "full" => DecorationMode::Full,
                    _ => DecorationMode::Auto,
                }
            } else if arg == "--with-notes" {
                flags.with_notes = true;
            } else {
                return Err(format!("Uknown option '{}'", arg));
            }
        }

        Ok((args_without_switches, flags))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_grouping() {
        let todos = vec![
            TodoItem {
                id: 1,
                subject: "Something I want to do".into(),
                contexts: ["go".into(), "programming".into()].to_vec(),
                projects: ["server".into(), "client".into()].to_vec(),
                ..TodoItem::default()
            },
            TodoItem {
                id: 2,
                subject: "Something else".into(),
                contexts: ["rust".into(), "programming".into()].to_vec(),
                projects: ["cli".into(), "tui".into()].to_vec(),
                ..TodoItem::default()
            },
            TodoItem {
                id: 3,
                subject: "Something else 1".into(),
                contexts: ["C".into(), "programming".into()].to_vec(),
                projects: ["embedded".into()].to_vec(),
                ..TodoItem::default()
            },
            TodoItem {
                id: 4,
                subject: "Something unrelated".into(),
                contexts: ["security".into(), "development".into()].to_vec(),
                projects: ["cryptography".into(), "currencies".into()].to_vec(),
                ..TodoItem::default()
            },
        ];

        let mut grouped: BTreeMap<String, Vec<&TodoItem>> = BTreeMap::new();
        let mut contexts: Vec<String> =
            todos.iter().map(|x| x.contexts.clone()).flatten().collect();
        contexts.sort();
        contexts.dedup();
        for context in contexts.iter() {
            grouped.insert(
                context.clone(),
                todos
                    .iter()
                    .filter(|x| x.contexts.iter().any(|y| y == context.as_str()))
                    .collect(),
            );
        }

        assert_eq!(group_by(&todos, Some(Group::Context)), grouped);
    }

    #[test]
    fn test_finding_next_id() {
        let a = [25, 1, 15, 13, 7, 6, 55, 12, 3, 2];
        assert_eq!(next_id(&a), 4);
    }

    #[test]
    fn test_finding_next_id_in_empty_array() {
        let a = [];
        assert_eq!(next_id(&a), 1);
    }

    #[test]
    fn test_finding_next_id_last() {
        let a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        assert_eq!(next_id(&a), 11);
    }

    #[test]
    fn test_finding_next_id_sorted() {
        let a = [1, 2, 3, 4, 5, 7, 8, 9, 10];
        assert_eq!(next_id(&a), 6);
    }

    #[test]
    fn test_filter_parsing_full_project() {
        let args: Vec<String> = ["todos", "--project=rpn_calc"]
            .iter()
            .map(|x| x.to_string())
            .collect();
        let (_, flags) = Flags::from_args(&mut args.into_iter()).unwrap();
        assert_eq!(flags.projects, Some(vec!["rpn_calc".into()]));
    }

    #[test]
    fn test_project_short_flag() {
        let args: Vec<String> = ["todos", "-p", "rpn_calc"]
            .iter()
            .map(|x| x.to_string())
            .collect();
        let (_, flags) = Flags::from_args(&mut args.into_iter()).unwrap();
        assert_eq!(flags.projects, Some(vec!["rpn_calc".into()]));
    }

    #[test]
    fn test_filter_parsing_projec() {
        let args: Vec<String> = ["todos", "--p=rpn_calc"]
            .iter()
            .map(|x| x.to_string())
            .collect();
        let (_, flags) = Flags::from_args(&mut args.into_iter()).unwrap();
        assert_eq!(flags.projects, Some(vec!["rpn_calc".into()]));
    }

    #[test]
    fn test_filter_parsing_context() {
        let args: Vec<String> = ["todos", "--con=go"]
            .iter()
            .map(|x| x.to_string())
            .collect();
        let (_, flags) = Flags::from_args(&mut args.into_iter()).unwrap();
        assert_eq!(flags.contexts, Some(vec!["go".into()]));
    }

    #[test]
    fn test_filter_parsing_context_short() {
        let args: Vec<String> = ["todos", "-c", "go"]
            .iter()
            .map(|x| x.to_string())
            .collect();
        let (_, flags) = Flags::from_args(&mut args.into_iter()).unwrap();
        assert_eq!(flags.contexts, Some(vec!["go".into()]));
    }

    #[test]
    fn test_filtering_grouping_together() {
        let args: Vec<String> = ["todos", "--con=rust", "-p", "rpn_calc", "-g", "project"]
            .iter()
            .map(|x| x.to_string())
            .collect();
        let (_, flags) = Flags::from_args(&mut args.into_iter()).unwrap();
        assert_eq!(flags.contexts, Some(vec!["rust".into()]));
        assert_eq!(flags.projects, Some(vec!["rpn_calc".into()]));
        assert_eq!(flags.group_by, Some(Group::Project))
    }

    #[test]
    fn test_filter_completed() {
        let args: Vec<String> = ["todos", "--completed"]
            .iter()
            .map(|x| x.to_string())
            .collect();
        let (_, flags) = Flags::from_args(&mut args.into_iter()).unwrap();
        assert_eq!(flags.completed, Some(true));
    }

    #[test]
    fn test_folding() {
        let folded = fold("This is a line with a lot of text", 10);
        assert_eq!(&folded, &["This is a ", "line with ", "a lot of ", "text "]);
    }

    #[test]
    fn test_folding_with_limit_smaller_than_len() {
        let folded = fold("This is a line with a lot of text", 3);
        assert_eq!(
            &folded,
            &[
                "Thi…", "s ", "is ", "a ", "lin…", "e ", "wit…", "h ", "a ", "lot ", "of ", "tex…",
                "t "
            ]
        );
    }

    #[test]
    fn test_filter_by_context() {
        let todos = vec![
            TodoItem {
                id: 1,
                subject: String::from("Some task"),
                projects: vec![String::from("Project1")],
                contexts: vec![String::from("go")],
                ..TodoItem::default()
            },
            TodoItem {
                id: 2,
                subject: String::from("Change the color of the room"),
                projects: vec![String::from("Home renovation")],
                ..TodoItem::default()
            },
            TodoItem {
                id: 3,
                subject: String::from("Fix the bathroom"),
                projects: vec![String::from("Home renovation")],
                ..TodoItem::default()
            },
        ];
        let flags = Flags {
            contexts: Some(vec!["go".to_owned()]),
            ..Flags::default()
        };
        let expected = vec![todos[0].clone()];
        let filtered = filter_todos(&todos, &flags);
        assert_eq!(filtered, expected);
    }

    #[test]
    fn test_filter() {
        let todos = vec![
            TodoItem {
                id: 1,
                subject: String::from("Some task"),
                projects: vec![String::from("Project1")],
                ..TodoItem::default()
            },
            TodoItem {
                id: 2,
                subject: String::from("Change the color of the room"),
                projects: vec![String::from("Home renovation")],
                ..TodoItem::default()
            },
            TodoItem {
                id: 3,
                subject: String::from("Fix the bathroom"),
                projects: vec![String::from("Home renovation")],
                ..TodoItem::default()
            },
        ];
        let flags = Flags {
            projects: Some(vec!["Project1".to_owned()]),
            ..Flags::default()
        };
        let expected = vec![todos[0].clone()];
        let filtered = filter_todos(&todos, &flags);
        assert_eq!(filtered, expected);
    }

    #[test]
    fn test_mutilple_filters() {
        let todos = vec![
            TodoItem {
                id: 1,
                subject: String::from("Some task"),
                projects: vec![String::from("Project1")],
                ..TodoItem::default()
            },
            TodoItem {
                id: 2,
                subject: String::from("Change the color of the room"),
                projects: vec![String::from("Home renovation")],
                completed: true,
                ..TodoItem::default()
            },
            TodoItem {
                id: 3,
                subject: String::from("Fix the bathroom"),
                projects: vec![String::from("Home renovation")],
                ..TodoItem::default()
            },
        ];
        let flags = Flags {
            projects: Some(vec!["Home renovation".to_owned()]),
            completed: Some(true),
            ..Flags::default()
        };
        let expected = vec![todos[1].clone()];
        let filtered = filter_todos(&todos, &flags);
        assert_eq!(filtered, expected);
    }

    #[test]
    fn test_multiple_filters() {
        let todos = vec![
            TodoItem {
                id: 1,
                subject: String::from("Some task"),
                projects: vec![String::from("Project1")],
                ..TodoItem::default()
            },
            TodoItem {
                id: 2,
                subject: String::from("Change the color of the room"),
                projects: vec![String::from("Home renovation")],
                completed: true,
                ..TodoItem::default()
            },
            TodoItem {
                id: 3,
                subject: String::from("Fix the bathroom"),
                projects: vec![String::from("Home renovation")],
                ..TodoItem::default()
            },
        ];
        let flags = Flags {
            projects: Some(vec!["Home renovation".to_owned()]),
            completed: Some(true),
            ..Flags::default()
        };
        let expected = vec![todos[1].clone()];
        let filtered = filter_todos(&todos, &flags);
        assert_eq!(filtered, expected);
    }

    #[test]
    fn test_filter_multiple() {
        let todos = vec![
            TodoItem {
                id: 1,
                subject: String::from("Some task"),
                projects: vec![String::from("Project1")],
                ..TodoItem::default()
            },
            TodoItem {
                id: 2,
                subject: String::from("Change the color of the room"),
                projects: vec![String::from("Home renovation")],
                ..TodoItem::default()
            },
            TodoItem {
                id: 3,
                subject: String::from("Fix the bathroom"),
                projects: vec![String::from("Home renovation")],
                ..TodoItem::default()
            },
        ];
        let flags = Flags {
            projects: Some(vec!["Home renovation".into()]),
            ..Flags::default()
        };
        let expected = vec![todos[1].clone(), todos[2].clone()];
        let filtered = filter_todos(&todos, &flags);
        assert_eq!(filtered, expected);
    }
}
